<?php
namespace d84\Collection;

/**
 * Basic collection class
 */
class Collection implements \ArrayAccess, \IteratorAggregate
{
    /**
     * Сортирует массив, сохраняя ключи
     * @var string
     */
    const SORT_ASORT  = 'asort';

    /**
     * Сортирует массив в обратном порядке, сохраняя ключи
     * @var string
     */
    const SORT_ARSORT = 'arsort';

    /**
     * Сортирует массив по ключам
     * @var string
     */
    const SORT_KSORT  = 'ksort';

    /**
     * Сортирует массив по ключам в обратном порядке
     * @var string
     */
    const SORT_KRSORT = 'krsort';

    /**
     * Сортирует массив, используя алгоритм "natural order"
     * @var string
     */
    const SORT_NASORT = 'nasort';

    /**
     * Сортирует массив
     * @var string
     */
    const SORT_DEFAULT = 'sort';

    /**
     * Сортирует массив в обратном порядке
     * @var string
     */
    const SORT_RSORT  = 'rsort';

    /**
     * Сортировка таблицы
     * @var string
     */
    const SORT_TABLE  = 'tblsort';

    /** @var array */
    private $collection;

    /**
     * @param  array|Collection $collection
     */
    public function __construct($collection)
    {
        $this->setAll($collection);
    }

    /**
     * @param  Collection|array $collection
     *
     * @return self
     *
     * @throws \RuntimeException
     */
    public function merge($collection): self
    {
        $in_collection = [];
        if ($collection instanceof Collection) {
            $in_collection = $collection->getAll();
        } elseif (is_array($collection)) {
            $in_collection = $collection;
        } else {
            throw new \RuntimeException("Expected collection or array");
        }
        return self::fromArray(array_merge($this->collection, $in_collection));
    }

    /**
     * @return int
     */
    public function size(): int
    {
        return count($this->collection);
    }

    /**
     * @param  string|int $key
     * @param  mixed      $default
     *
     * @return mixed
     */
    public function get($key, $default = null)
    {
        $ret = $default;

        if ($this->has($key)) {
            $ret = $this->collection[$key];
        }

        return $ret;
    }

    /**
     * @param  string|int $key
     * @param  int|null   $default
     *
     * @return int
     */
    public function getInt($key, int $default = null): int
    {
        return $this->getWithType($key, 'int', $default);
    }

    /**
     * @param  string|int $key
     * @param  float|null $default
     *
     * @return float
     */
    public function getFloat($key, float $default = null): float
    {
        return $this->getWithType($key, 'float', $default);
    }

    /**
     * @param  string|int  $key
     * @param  string|null $default
     *
     * @return string
     */
    public function getString($key, string $default = null): string
    {
        return $this->getWithType($key, 'string', $default);
    }

    /**
     * @param  string|int  $key
     * @param  bool|null   $default
     *
     * @return bool
     */
    public function getBool($key, bool $default = null): bool
    {
        return $this->getWithType($key, 'bool', $default);
    }

    /**
     * @param  string|int   $key
     * @param  array|null   $default
     *
     * @return array
     */
    public function getArray($key, array $default = null): array
    {
        return $this->getWithType($key, 'array', $default);
    }

    /**
     * @param  string|int $key  Key or path
     *
     * @return self
     */
    public function collection($key)
    {
        $ret = [];
        if (strpos($key, '/') === false) {
            if ($this->has($key)) {
                $ret = $this->get($key);
                if ($ret instanceof Collection) {
                    $ret = $ret->getAll();
                } elseif (!is_array($ret)) {
                    $ret = [$ret];
                }
            }
        } else {
            if (is_string($key)) {
                $ret = $this->find($key);
            }
        }
        return self::fromArray($ret);
    }

    /**
     * @param  string|int   $key
     * @param  string|array $type     Allowed values: int, bool, float, string, array, object
     * @param  mixed        $default
     *
     * @return mixed
     */
    public function getWithType($key, $type, $default = null)
    {
        $val = $this->get($key, null);

        if (is_null($val)) {
            return $default;
        }

        $this->checkType($this->getType($val), $type);

        return $val;
    }

    /**
     * @param  string|int $key
     * @param  mixed      $value
     * @param  bool       $overwrite
     *
     * @return self
     *
     * @throws  \RuntimeException
     */
    public function set($key, $value, bool $overwrite = true): self
    {
        if ($overwrite == false && $this->has($key)) {
            throw new \RuntimeException("Key '$key' already exists, unable to overwrite");
        }

        $this->collection[$key] = $value;
        return $this;
    }

    /**
     * @param  mixed $value
     *
     * @return self
     */
    public function add($value): self
    {
        $this->collection[] = $value;
        return $this;
    }

    /**
     * @param  string|int $key
     *
     * @return bool
     */
    public function has($key): bool
    {
        return array_key_exists($key, $this->collection);
    }

    /**
     * @param  mixed $val
     *
     * @return bool
     */
    public function contains($val): bool
    {
        return in_array($val, $this->collection);
    }

    /**
     * @param  string|int $key
     *
     * @return bool
     */
    public function delete($key): bool
    {
        if ($this->has($key)) {
            unset($this->collection[$key]);
            return true;
        }
        return false;
    }

    /**
     * Finds value by path (Example: /this/is/my/value/path)
     *
     * @param  string $path
     *
     * @return array|null
     */
    public function find(string $path)
    {
        try {
            $result = $this->findPath($this->collection, explode('/', $path));

            if (!is_array($result)) {
                $result = [$result];
            }
        } catch (\RuntimeException $e) {
            return null;
        }

        return $result;
    }

    /**
     * @param array|Collection $collection
     */
    public function setAll($collection): void
    {
        if ($collection instanceof Collection) {
            $this->collection = $collection->getAll();
        } else {
            $this->collection = $collection;
        }
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return $this->collection;
    }


    /**
     * @param  mixed $search_value
     * @param  bool  $ascollection
     *
     * @return array|self
     */
    public function keys($search_value = null, bool $ascollection = true)
    {
        $r = [];

        if (is_null($search_value)) {
            $r = array_keys($this->collection);
        } else {
            $r = array_keys($this->collection, $search_value);
        }

        if ($ascollection) {
            $r = self::fromArray($r);
        }

        return $r;
    }

    /**
     * @param  bool  $ascollection
     *
     * @return array|self
     */
    public function values(bool $ascollection = true)
    {
        $r = array_values($this->collection);

        if ($ascollection) {
            $r = self::fromArray($r);
        }

        return $r;
    }

    /**
     * Searches the array for a given value and returns the first corresponding key if successful
     * Returns the key for value if it is found in the Collection, FALSE otherwise.
     *
     * @param  mixed $value
     *
     * @return mixed
     */
    public function search($value)
    {
        return array_search($value, $this->collection);
    }

    /**
     * Returns the values from a single column in the input array
     *
     * @param  string $column
     * @param  mixed  $column_index
     * @param  bool   $ascollection
     *
     * @return array|self
     */
    public function column(string $column, $column_index = null, bool $ascollection = true)
    {
        $r = [];

        if (is_null($column_index)) {
            $r = array_column($this->collection, $column);
        } else {
            $r = array_column($this->collection, $column, $column_index);
        }

        if ($ascollection) {
            $r = self::fromArray($r);
        }

        return $r;
    }

    /**
     * callaback - mixed callback ( mixed $carry , mixed $item )
     *
     * carry
     * Holds the return value of the previous iteration; in the case of the first iteration it instead holds the value of initial.
     *
     * item
     * Holds the value of the current iteration.
     *
     * @param  callable $callback
     * @param  mixed    $initial
     *
     * @return mixed
     */
    public function reduce(callable $callback, $initial = null)
    {
        return array_reduce($this->collection, $callback, $initial);
    }

    /**
     * @param  string $op      Operator, allowed one of the folliwing: +,-,*,/,%
     * @param  mixed  $initial
     *
     * @return mixed
     */
    public function reduceWith(string $op, $initial = null)
    {
        $reducer = function ($carry, $item) use ($op) {
            switch ($op) {
                case '-':
                    return $carry -= $item;
                case '+':
                    return $carry += $item;
                case '*':
                    return $carry *= $item;
                case '%':
                    return $carry %= $item;
            }
        };
        return $this->reduce($reducer, $initial);
    }

    /**
     * @param  string   $sort_mode
     * @param  int|null $flags
     * @param  array    $options
     *
     * @return self|bool
     */
    public function sort($sort_mode = self::SORT_DEFAULT, int $flags = null, array $options = [])
    {
        $sort_mode = strtolower($sort_mode);
        $this->checkSortMode($sort_mode);
        $ret = false;

        if ($sort_mode == self::SORT_TABLE) {
            $ret = $this->sortTable($this->collection, $options['column'] ?? '');
        } else {
            if (is_null($flags)) {
                $ret = $sort_mode($this->collection);
            } else {
                $ret = $sort_mode($this->collection, $flags);
            }
        }

        if ($ret === false) {
            return false;
        }

        return self::fromArray($this->collection);
    }

    /**
     * @param  array|string $class
     * @param  bool         $retain_key
     *
     * @return self
     *
     * @throws \RuntimeException
     */
    public function of($class, bool $retain_key = false): self
    {
        $filterByClass = function ($c, $retain_key) {
            $ret = [];
            foreach ($this->eachOf($c) as $k => $v) {
                if ($retain_key) {
                    $ret[$k] = $v;
                } else {
                    $ret[] = $v;
                }
            }
            return $ret;
        };

        if (is_array($class)) {
            $ret = [];
            foreach ($class as $c) {
                $ret = array_merge($filterByClass($c, $retain_key), $ret);
            }
            return self::fromArray($ret);
        }

        if (is_string($class)) {
            return self::fromArray($filterByClass($class, $retain_key));
        }

        throw new \RuntimeException("Expected 'array' or 'string', but supplied '" . gettype($class) . "'");
    }

    /**
     * @return self
     */
    public function numbers(): self
    {
        $ret = [];
        foreach ($this->eachNumber() as $k => $v) {
            $ret[] = $v;
        }
        return self::fromArray($ret);
    }

    /**
     * @return self
     */
    public function strings(): self
    {
        $ret = [];
        foreach ($this->eachString() as $k => $v) {
            $ret[] = $v;
        }
        return self::fromArray($ret);
    }

    /**
     * @return self
     */
    public function objects(): self
    {
        $ret = [];
        foreach ($this->eachObject() as $k => $v) {
            $ret[] = $v;
        }
        return self::fromArray($ret);
    }

    /**
     * @return self
     */
    public function arrays(): self
    {
        $ret = [];
        foreach ($this->eachArray() as $k => $v) {
            $ret[] = $v;
        }
        return self::fromArray($ret);
    }

    /**
     * @param  string $glue
     * @return string
     */
    public function implode(string $glue = ''): string
    {
        return implode($glue, $this->collection);
    }

    /**
     * @param  callable   $f
     * @param  bool       $retain_key
     *
     * @return Collection
     */
    public function when(callable $f, bool $retain_key = false): Collection
    {
        $out = [];

        foreach ($this->filter($f) as $k => $v) {
            if ($retain_key) {
                $out[$k] = $v;
            } else {
                $out[] = $v;
            }
        }

        return Collection::fromArray($out);
    }

    /*
      ------------------------------------------------------------------------------------------------------------------
      Magic
      ------------------------------------------------------------------------------------------------------------------
     */

    /**
     * @return string
     */
    public function __toString(): string
    {
        // TODO: Very dumb
        return implode(',', $this->collection);
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function __set(string $name, $value): void
    {
        $this->collection[$name] = $value;
    }

    /**
     * @param  string $name
     *
     * @return mixed
     */
    public function __get(string $name)
    {
        if (!$this->has($name)) {
            return null;
        }
        if (is_array($this->collection[$name])) {
            $new_col = new self([]);
            $new_col->collection = &$this->collection[$name];
            return $new_col;
        }
        return $this->collection[$name];
    }

    /**
     * @param  string  $name
     *
     * @return bool
     */
    public function __isset(string $name): bool
    {
        return $this->has($name);
    }

    /**
     * @param string $name
     */
    public function __unset(string $name): void
    {
        $this->delete($name);
    }

    /*
      ------------------------------------------------------------------------------------------------------------------
      Generators
      ------------------------------------------------------------------------------------------------------------------
     */

    /**
     * @param callable $callback
     */
    public function each(callable $callback): void
    {
        $iterator = $this->getIterator();
        foreach ($iterator as $key => $value) {
            $callback($value, $key);
        }
    }

    /**
     * @param  string $class
     */
    public function eachOf(string $class)
    {
        $isInstanceOf = function ($item) use ($class) {
            return $item instanceof $class;
        };

        foreach ($this->filter($isInstanceOf) as $k => $item) {
            yield $k => $item;
        }
    }

    /**
     */
    public function eachString()
    {
        $isString = function ($item) {
            return is_string($item);
        };

        foreach ($this->filter($isString) as $k => $item) {
            yield $k => $item;
        }
    }

    /**
     */
    public function eachNumber()
    {
        $isNumeric = function ($item) {
            return is_numeric($item);
        };

        foreach ($this->filter($isNumeric) as $k => $item) {
            yield $k => $item;
        }
    }

    /**
     */
    public function eachObject()
    {
        $isObject = function ($item) {
            return is_object($item);
        };

        foreach ($this->filter($isObject) as $k => $item) {
            yield $k => $item;
        }
    }

    /**
     */
    public function eachArray()
    {
        $isArray = function ($item) {
            return is_array($item);
        };

        foreach ($this->filter($isArray) as $k => $item) {
            yield $k => $item;
        }
    }

    /**
     */
    public function eachBool()
    {
        $isArray = function ($item) {
            return is_bool($item);
        };

        foreach ($this->filter($isArray) as $k => $item) {
            yield $k => $item;
        }
    }

    /**
     * @param callable $callback
     */
    public function map(callable $callback)
    {
        $iterator = $this->getIterator();
        foreach ($iterator as $key => $value) {
            yield $key => $callback($value);
        }
    }

    /**
     * @param  callable $callback
     */
    public function filter(callable $callback)
    {
        $iterator = $this->getIterator();
        foreach ($iterator as $key => $value) {
            if ($callback($value, $key)) {
                yield $key => $value;
            }
        }
    }

    /*
      ------------------------------------------------------------------------------------------------------------------
      ArrayAccess methods
      ------------------------------------------------------------------------------------------------------------------
     */

    /**
     * @param  mixed $offset
     * @return bool
     */
    public function offsetExists($offset): bool
    {
        return $this->has($offset);
    }

    /**
     * @param mixed $offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return $this->get($offset, null);
    }

    /**
     * @param mixed $offset
     * @param void $value
     */
    public function offsetSet($offset, $value): void
    {
        $this->set($offset, $value);
    }

    /**
     * @param mixed $offset
     */
    public function offsetUnset($offset): void
    {
        $this->delete($offset);
    }

    /*
      ------------------------------------------------------------------------------------------------------------------
      IteratorAggregate
      ------------------------------------------------------------------------------------------------------------------
     */

    /**
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->collection);
    }

    /*
      ------------------------------------------------------------------------------------------------------------------
      Static helpers
      ------------------------------------------------------------------------------------------------------------------
     */

    /**
     * @param  array $arr
     *
     * @return self
     */
    public static function fromArray(array $arr): self
    {
        return new self($arr);
    }

    /**
     * @param  Collection $col
     *
     * @return self
     */
    public static function fromCollection(Collection $col): self
    {
        return new self($col);
    }

    /**
     * @param  mixed $object
     * @param  bool  $use_getters
     *
     * @return self
     */
    public static function fromObject($object, bool $use_getters = false): self
    {
        $properties = [];

        if ($use_getters) {
            $isGetter = function ($method_name) {
                return strpos($method_name, 'get') === 0;
            };

            foreach (self::fromArray(get_class_methods($object))->filter($isGetter) as $getter) {
                $property = lcfirst(substr($getter, 3));
                $properties[$property] = $object->$getter();
            }
        }

        $properties = array_merge($properties, get_object_vars($object));

        return new self($properties);
    }

    /**
     * @see parse_str
     *
     * @param  string $string
     *
     * @return self
     */
    public static function fromString(string $string): self
    {
        parse_str($string, $out);
        return self::fromArray($out);
    }

    /**
     * @see str_getcsv
     *
     * @param  string $csv
     * @param  string $delimiter
     *
     * @return self
     */
    public static function fromCsv(string $csv, string $delimiter = ','): self
    {
        return self::fromArray(str_getcsv($csv, $delimiter));
    }

    /**
     * @param  array $keys
     * @param  array $values
     *
     * @return self
     *
     * @throws \RuntimeException
     */
    public static function combine(array $keys, array $values): self
    {
        $combined = [];
        try {
            $combined = array_combine($keys, $values);
        } catch (\Exception $e) {
            throw new \RuntimeException("The arrays 'keys' and 'values' must be an equal size");
        }
        return self::fromArray($combined);
    }

    /*
      ------------------------------------------------------------------------------------------------------------------
      Private helpers
      ------------------------------------------------------------------------------------------------------------------
     */

    /**
     * @param  array  $array
     * @param  string $column
     * @return bool
     */
    private function sortTable(array $array, string $column): bool
    {
        if (empty($column)) {
            // Not defined column name
            return false;
        }

        return usort($this->collection, function ($a, $b) use ($column) {
            return $a[$column] <=> $b[$column];
        });
    }

    /**
     * @param string $sort_mode
     */
    private function checkSortMode(string $sort_mode): void
    {
        $allowed = [
           self::SORT_ASORT,
           self::SORT_ARSORT,
           self::SORT_KSORT,
           self::SORT_KRSORT,
           self::SORT_NASORT,
           self::SORT_DEFAULT,
           self::SORT_RSORT,
           self::SORT_TABLE
         ];

        if (!in_array($sort_mode, $allowed)) {
            throw new \RuntimeException("Unknown sort mode '$sort_mode'");
        }
    }

    /**
     * @param  string       $value_type
     * @param  string|array $expected_type
     *
     * @return void
     *
     * @throws \RuntimeException
     */
    private function checkType(string $value_type, $expected_type): void
    {
        if (is_array($expected_type)) {
            if (in_array($value_type, $expected_type)) {
                return;
            }
            $expected = implode(',', $expected_type);
            throw new \RuntimeException("Type mismatch: expected '$expected', but supplied '$value_type'");
        }
        if (is_string($expected_type)) {
            if ($expected_type == $value_type) {
                return;
            }
            throw new \RuntimeException("Type mismatch: expected '$expected_type', but supplied '$value_type'");
        }
        throw new \RuntimeException("The value of 'expected_type' parameter must be either string or array");
    }

    /**
     * @param  array  $node
     * @param  array  $path
     *
     * @return mixed
     *
     * @throws \RuntimeException
     */
    private function findPath(array $node, array $path)
    {
        if (!is_array($node)) {
            throw new \RuntimeException('Node not array');
        }

        $next_node = null;
        $next_key  = array_shift($path);

        if (is_null($next_key) || !array_key_exists($next_key, $node)) {
            throw new \RuntimeException('Not found node');
        }

        $next_node = $node[$next_key];

        if (empty($path)) {
            return $next_node;
        }

        return $this->findPath($next_node, $path);
    }

    /**
     * @param  mixed $val
     *
     * @return string
     */
    private function getType($val): string
    {
        $php_type = strtolower(gettype($val));
        if ($php_type === 'double') {
            return 'float';
        }
        if ($php_type === 'boolean') {
            return 'bool';
        }
        if ($php_type === 'integer') {
            return 'int';
        }
        return $php_type;
    }
}
