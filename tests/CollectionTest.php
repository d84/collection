<?php
use PHPUnit\Framework\TestCase;

use d84\Collection\Collection;

class User
{
    private $id = 1234;
    private $name = 'Tom';
    private $roles = ['User', 'Publisher'];
    public $comment = 'This is comment';

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getRoles()
    {
        return $this->roles;
    }
}

/**
 * Collection test
 */
class CollectionTest extends TestCase
{
    /** @var Collection */
    protected static $num_col;

    /** @var Collection */
    protected static $str_col;

    protected static $products;

    public static function setUpBeforeClass()
    {
        self::$num_col = Collection::fromArray(
          [1,2,3,4,5,6,7,8,9,10,0.5,0.34]
        );

        self::$str_col = Collection::fromArray(
          [
            'one' => 1,
            'two' => 2,
            'three' => 3,
            'array' => [11,22,33,44],
            'negative' => false,
            'nullable' => null,
            'msg' => 'Hello!',
            'test' => [
              'path' => [
                'with' => null
              ]
            ],
            'product' => [
              'id' => 123,
              'subproducts' => [
                'milk',
                'apple',
                'egg'
              ]
            ]
          ]
        );

        self::$products = [
          ['id' => 100, 'name' => 'Soap', 'description' => ''],
          ['id' => 232, 'name' => 'Pen', 'description' => ''],
          ['id' => 31, 'name' => 'Toy', 'description' => ''],
          ['id' => 3, 'name' => 'Asteriks', 'description' => '']
        ];
    }

    /**
     * @test
     */
    public function mergeWithArray()
    {
        $col1 = Collection::fromArray([1,2]);
        $col2 = [3,4];
        $col3 = $col1->merge($col2);
        $this->assertEquals([1,2,3,4], $col3->getAll());
    }

    /**
     * @test
     */
    public function mergeWithCollection()
    {
        $col1 = Collection::fromArray([1,2]);
        $col2 = Collection::fromArray([3,4]);
        $col3 = $col1->merge($col2);
        $this->assertEquals([1,2,3,4], $col3->getAll());
    }

    /**
     * @test
     */
    public function arrayAccess()
    {
        $this->assertEquals(22, self::$str_col['array'][1]);
    }

    /**
     * @test
     */
    public function keys()
    {
        $keys = [
          'one' ,
          'two' ,
          'three' ,
          'array' ,
          'negative',
          'nullable',
          'msg' ,
          'test' ,
          'product'
        ];
        $this->assertEquals($keys, self::$str_col->keys()->getAll());
    }

    /**
     * @test
     */
    public function values()
    {
        $col = Collection::fromArray([
          'a' => [
            'b' => [1, 2, 3]
          ],
          'c' => [
            'd' => [2, 3, 4]
          ]
        ]);

        $this->assertEquals([1,2,3], $col->values()->collection('0/b')->getAll());
    }

    /**
     * @test
     */
    public function combine()
    {
        $col = Collection::combine(['a', 'b', 'c', 'd'], ['aa', 'bb', 'cc', 'dd']);
        $this->assertEquals('c', $col->search('cc'));
    }

    /**
     * @test
     * @expectedException \RuntimeException
     * @expectedExceptionMessage The arrays 'keys' and 'values' must be an equal size
     */
    public function combineException()
    {
        $col = Collection::combine(['a', 'b', 'c', 'd', 'g'], ['aa', 'bb', 'cc', 'dd']);
        $this->assertEquals('c', $col->search('cc'));
    }

    /**
     * @test
     */
    public function reduce()
    {
        $col = Collection::fromArray([1,2,3,4,5,6,7,8,9]);

        $mul = function ($carry, $item) {
            return $carry *= $item;
        };

        $reduced = $col->reduce($mul, 1);

        $this->assertEquals(362880, $reduced);
    }

    /**
     * @test
     */
    public function reduceWith()
    {
        $reduced = Collection::fromArray([1,2,3,4,5,6,7,8,9])->reduceWith('*', 1);
        $this->assertEquals(362880, $reduced);
    }

    /**
     * @test
     */
    public function column()
    {
        $pen_row_id = Collection::fromArray(self::$products)->column('name')->search('Pen');
        $this->assertEquals(1, $pen_row_id);
    }

    /**
     * @test
     */
    public function columnWithIndex()
    {
        $pen_id = Collection::fromArray(self::$products)->column('name', 'id')->search('Pen');
        $this->assertEquals(232, $pen_id);
    }

    /**
     * @test
     */
    public function sortAsTable()
    {
        $col = Collection::fromArray(self::$products)->sort(Collection::SORT_TABLE, null, ['column' => 'name']);
        $this->assertEquals(3, $col[0]['id']);
    }

    /**
     * @test
     */
    public function fromObject()
    {
        $user = new User();
        $col = Collection::fromObject($user, true);

        $this->assertEquals(1234, $col->get('id'));
        $this->assertEquals('Tom', $col['name']);
        $this->assertEquals(['User', 'Publisher'], $col['roles']);
        $this->assertEquals('This is comment', $col->comment);
    }

    /**
     * @test
     */
    public function fromCsv()
    {
        $col = Collection::fromCsv('1,2,3,4,5');

        $this->assertEquals(15, $col->reduceWith('+', 0));
    }

    /**
     * @test
     */
    public function implodeNumbers()
    {
        $string = Collection::fromCsv('1,2,"t",null,0,"b"');
        $this->assertEquals('120', $string->numbers()->implode());
    }

    /**
     * @test
     */
    public function fromStr()
    {
        $col = Collection::fromString('first=value&arr[]=foo+bar&arr[]=baz');

        $this->assertEquals('baz', $col->collection('arr')->get(1));
    }

    /**
     * @test
     */
    public function when()
    {
        $col = Collection::fromArray([1,2,3,4,5,6,7,8,9,0]);

        $col = $col->when(function ($value) {
            return $value % 2 == 0;
        });

        $this->assertEquals(5, $col->size());
    }
}
