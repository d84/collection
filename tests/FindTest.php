<?php
use PHPUnit\Framework\TestCase;

use d84\Collection\Collection;

/**
 * find
 * search
 * contains
 */
class FindTest extends TestCase
{
    /** @var Collection */
    protected static $num_col;

    /** @var Collection */
    protected static $str_col;

    public static function setUpBeforeClass()
    {
        self::$num_col = Collection::fromArray(
          [1,2,3,4,5,6,7,8,9,10,0.5,0.34]
        );

        self::$str_col = Collection::fromArray(
          [
            'one' => 1,
            'two' => 2,
            'three' => 3,
            'array' => [11,22,33,44],
            'negative' => false,
            'nullable' => null,
            'msg' => 'Hello!',
            'test' => [
              'path' => [
                'with' => null
              ]
            ],
            'product' => [
              'id' => 123,
              'subproducts' => [
                'milk',
                'apple',
                'egg'
              ]
            ]
          ]
        );
    }

    /**
     * @test
     */
    public function find()
    {
        $results = self::$str_col->find('product/subproducts');
        $this->assertEquals('apple', $results[1]);
    }

    /**
     * @test
     */
    public function notFoundPath()
    {
        $results = self::$str_col->find('product/subproducts/test/me/unkown/path');
        $this->assertEquals(null, $results);
    }

    /**
     * @test
     */
    public function findNullValue()
    {
        $results = self::$str_col->find('test/path/with');
        $this->assertEquals(null, $results[0]);
    }

    /**
     * @test
     */
    public function containsValue()
    {
        $this->assertEquals(true, self::$str_col->collection('product/subproducts')->contains('egg'));
    }

    /**
     * @test
     */
    public function search()
    {
        $this->assertEquals('array', self::$str_col->search([11,22,33,44]));
    }
}
