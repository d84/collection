<?php
use PHPUnit\Framework\TestCase;

use d84\Collection\Collection;

/**
 * map
 * filter
 * each
 * iterator
 */
class GeneratorTest extends TestCase
{
    /**
     * @test
     */
    public function iterator()
    {
        $col_iterator = Collection::fromArray([1,2,3])->getIterator();
        $sum = 0;
        foreach ($col_iterator as $v) {
            $sum += $v;
        }
        $this->assertEquals(6, $sum);
    }

    /**
     * @test
     */
    public function each()
    {
        $col = Collection::fromArray([1,2,3]);
        $sum = 0;
        $col->each(function ($value) use (&$sum) {
            $sum += $value;
        });
        $this->assertEquals(6, $sum);
    }

    /**
     * @test
     */
    public function map()
    {
        // Find all odd
        $col = Collection::fromArray([1,2,3,3,4,5,6,7,8,9,10]);
        $result = [];
        $odd = function ($value) {
            return $value % 2 == 0;
        };

        foreach ($col->map($odd) as $k => $v) {
            if ($v === true) {
                $result[] = $k;
            }
        }

        $this->assertEquals(5, count($result));
    }

    /**
     * @test
     */
    public function filter()
    {
        // Find all elements that are start from the 'a' character
        $col = Collection::fromArray(['aa', 'ab', 'cb', 'db', 'al', 'ol', 'aq']);
        $result = [];

        $start_from_a_filter = function ($value) {
            return $value[0] === 'a';
        };

        foreach ($col->filter($start_from_a_filter) as $v) {
            $result[] = $v;
        }

        $this->assertEquals(['aa', 'ab', 'al', 'aq'], $result);
    }

    /**
     * @test
     */
    public function eachOf()
    {
        $col = Collection::fromArray(['aa', 1, true, null, new \Exception("Test"), [1,2,3], 'ol', 0.5]);

        $key = 0;
        $val = 0;

        foreach ($col->eachOf(\Exception::class) as $k => $item) {
            // DO USE ORIGINAL KEY
            $key = $k;
            $val = $item->getMessage();
        }

        $this->assertEquals(4, $key);
        $this->assertEquals("Test", $val);
    }

    /**
     * @test
     */
    public function eachString()
    {
        $col = Collection::fromArray(['aa', 1, true, null, new \Exception("Test"), [1,2,3], 'ol', 0.5]);

        $strings = [];

        foreach ($col->eachString() as $k => $item) {
            // DO USE ORIGINAL KEY
            $strings[$k] = $item;
        }

        $this->assertEquals('aa', $strings[0]);
        $this->assertEquals('ol', $strings[6]);
        $this->assertEquals(2, count($strings));
    }

    /**
     * @test
     */
    public function eachNumber()
    {
        $col = Collection::fromArray(['aa', 1, true, null, new \Exception("Test"), [1,2,3], 'ol', 0.5]);

        $numbers = [];

        foreach ($col->eachNumber() as $k => $item) {
            // DO USE ORIGINAL KEY
            $numbers[$k] = $item;
        }

        $this->assertEquals(1, $numbers[1]);
        $this->assertEquals(0.5, $numbers[7]);
        $this->assertEquals(2, count($numbers));
    }

    /**
     * @test
     */
    public function eachObject()
    {
        $col = Collection::fromArray(['aa', 1, true, null, new \Exception("This is test"), [1,2,3], 'ol', 0.5]);

        $objects = [];

        foreach ($col->eachObject() as $k => $item) {
            // DO NOT ! USE KEY
            $objects[] = $item;
        }

        $this->assertEquals("This is test", $objects[0]->getMessage());
        $this->assertEquals(1, count($objects));
    }
}
