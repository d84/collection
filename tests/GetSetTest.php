<?php
use PHPUnit\Framework\TestCase;

use d84\Collection\Collection;

/**
 * Setters
 * Getters
 */
class GetSetTest extends TestCase
{
    /** @var Collection */
    protected static $num_col;

    /** @var Collection */
    protected static $str_col;

    public static function setUpBeforeClass()
    {
        self::$num_col = Collection::fromArray(
          [1,2,3,4,5,6,7,8,9,10,0.5,0.34]
        );

        self::$str_col = Collection::fromArray(
          [
            'one' => 1,
            'two' => 2,
            'three' => 3,
            'array' => [11,22,33,44],
            'negative' => false,
            'nullable' => null,
            'msg' => 'Hello!',
            'test' => [
              'path' => [
                'with' => null
              ]
            ],
            'product' => [
              'id' => 123,
              'subproducts' => [
                'milk',
                'apple',
                'egg'
              ]
            ]
          ]
        );
    }

    /**
     * @test
     */
    public function hasNumKey()
    {
        $this->assertTrue(self::$num_col->has(2));
    }

    /**
     * @test
     */
    public function hasStrKey()
    {
        $this->assertTrue(self::$str_col->has('two'));
    }

    /**
     * @test
     */
    public function getNumKey()
    {
        $this->assertEquals(3, self::$num_col->get(2));
    }

    /**
     * @test
     */
    public function getStrKey()
    {
        $this->assertEquals(3, self::$str_col->get('three'));
    }

    /**
     * @test
     */
    public function getNumInt()
    {
        $this->assertEquals(1, self::$num_col->getInt(0));
    }

    /**
     * @test
     * @expectedException \RuntimeException
     * @expectedExceptionMessage Type mismatch: expected 'int', but supplied 'float'
     */
    public function getNumIntException()
    {
        $this->assertEquals(3, self::$num_col->getInt(10));
    }

    /**
     * @test
     */
    public function getNumFloat()
    {
        $this->assertEquals(0.5, self::$num_col->getFloat(10));
    }

    /**
     * @test
     * @expectedException \RuntimeException
     * @expectedExceptionMessage Type mismatch: expected 'float', but supplied 'int'
     */
    public function getNumFloatException()
    {
        $this->assertEquals(3, self::$num_col->getFloat(3));
    }

    /**
     * @test
     */
    public function getString()
    {
        $this->assertEquals('Hello!', self::$str_col->getString('msg'));
    }

    /**
     * @test
     * @expectedException \RuntimeException
     * @expectedExceptionMessage Type mismatch: expected 'string', but supplied 'bool'
     */
    public function getStringException()
    {
        $this->assertEquals(3, self::$str_col->getString('negative'));
    }

    /**
     * @test
     */
    public function getBool()
    {
        $this->assertEquals(false, self::$str_col->getBool('negative'));
    }

    /**
     * @test
     * @expectedException \RuntimeException
     * @expectedExceptionMessage Type mismatch: expected 'bool', but supplied 'string'
     */
    public function getBoolException()
    {
        $this->assertEquals(3, self::$str_col->getBool('msg'));
    }

    /**
     * @test
     */
    public function getArray()
    {
        $this->assertEquals([11,22,33,44], self::$str_col->getArray('array'));
    }

    /**
     * @test
     * @expectedException \RuntimeException
     * @expectedExceptionMessage Type mismatch: expected 'array', but supplied 'string'
     */
    public function getArrayException()
    {
        $this->assertEquals(3, self::$str_col->getArray('msg'));
    }

    /**
     * @test
     */
    public function asCollection()
    {
        $this->assertEquals(4, self::$str_col->collection('array')->size());
    }

    /**
     * @test
     */
    public function asCollectionNoKey()
    {
        $this->assertEquals(0, self::$str_col->collection('UnknownKey')->size());
    }
}
