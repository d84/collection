<?php
use PHPUnit\Framework\TestCase;

use d84\Collection\Collection;

/**
 * __set
 * __get
 * __unset
 */
class MagicTest extends TestCase
{
    /**
     * @test
     */
    public function setValueMagic()
    {
        $col = Collection::fromArray(['User' => ['id' => 123, 'name' => 'Tom', 'email' => 'tom@dot.com']]);
        $col->User->email = '1@dot.com';
        $this->assertEquals('1@dot.com', $col->User->email);
    }

    /**
     * @test
     */
    public function getValueMagic()
    {
        $col = Collection::fromArray(
          ['one' =>'orange', 'two' => 'apple', 'three' => 'kiwi']
        );
        $this->assertEquals('apple', $col->two);
    }

    /**
     * @test
     */
    public function unsetValueMagic()
    {
        $col = Collection::fromArray(
          ['one' =>'orange', 'two' => 'apple', 'three' => 'kiwi']
        );
        unset($col->two);
        $this->assertEquals(2, $col->size());
    }
}
