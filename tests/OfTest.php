<?php
use PHPUnit\Framework\TestCase;

use d84\Collection\Collection;

class Car
{
}

class Ship
{
}

class Plane
{
}

/**
 * Of
 */
class OfTest extends TestCase
{
    /**
     * @test
     */
    public function collectionOf()
    {
        $col = Collection::fromArray(['a', new Car(), 1, 'b', new Ship() ,null, new Car(), true, new Plane(), ['a', 'b'], 1]);

        $cars = $col->of([Car::class]);
        $ships = $col->of([Ship::class]);
        $planes = $col->of([Plane::class]);

        $this->assertEquals(2, $cars->size());
        $this->assertEquals(1, $ships->size());
        $this->assertEquals(1, $planes->size());
    }

    /**
     * @test
     */
    public function objects()
    {
        $col = Collection::fromArray(['a', new Car(), 1, 'b', new Ship() ,null, new Car(), true, new Plane(), ['a', 'b'], 5]);

        $objects = $col->objects();

        $this->assertEquals(4, $objects->size());
    }

    /**
     * @test
     */
    public function numbers()
    {
        $col = Collection::fromArray(['a', new Car(), 1, 'b', new Ship() ,null, new Car(), true, new Plane(), ['a', 'b'], 5]);

        $numbers = $col->numbers();

        $this->assertEquals(2, $numbers->size());
        $this->assertEquals(6, $numbers->reduceWith('+', 0));
    }

    /**
     * @test
     */
    public function strings()
    {
        $col = Collection::fromArray(['a', new Car(), 1, 'b', new Ship() ,null, new Car(), true, new Plane(), ['a', 'b'], 5]);

        $strings = $col->strings();

        $this->assertEquals(2, $strings->size());
        $this->assertEquals('ab', $strings->implode());
    }

    /**
     * @test
     */
    public function arrays()
    {
        $col = Collection::fromArray(['a', new Car(), 1, 'b', new Ship() ,null, new Car(), true, new Plane(), ['a', 'b'], 5]);

        $arrays = $col->arrays();

        $this->assertEquals(1, $arrays->size());
        $this->assertEquals('ab', $arrays->collection(0)->implode());
    }

    /**
     * @test
     */
    public function of()
    {
        $col = Collection::fromArray(
          ['a', new Car(), 1, 'b', new Ship() ,null, new Car(), true, new Plane(), ['a', 'b'], 5, new Plane()]
        );

        $arrays = $col->of([Car::class, Plane::class]);

        $this->assertEquals(4, $arrays->size());
    }

    /**
     * @test
     */
    public function ofRetainKey()
    {
        $col = Collection::fromArray(
          ['a' => new Car(), 'b' => new Plane(), 'c' => new Car(), 'd' => 12]
        );

        $arrays = $col->of(Car::class, true);

        $this->assertEquals(2, $arrays->size());
        $this->assertEquals('a', $arrays->keys()[0]);
        $this->assertEquals('c', $arrays->keys()[1]);
    }
}
